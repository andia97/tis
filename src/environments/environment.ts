// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://df61242eb02f.ngrok.io/',
  //apiUrl: 'http://localhost:8080/',
  firebase: {
    apiKey: "AIzaSyDRQlvO_kgcVpQ8qe7HDLSbYUoWSNwnutE",
    authDomain: "tis-filecloud.firebaseapp.com",
    databaseURL: "https://tis-filecloud.firebaseio.com",
    projectId: "tis-filecloud",
    storageBucket: "tis-filecloud.appspot.com",
    messagingSenderId: "662069880625",
    appId: "1:662069880625:web:b43c54ad6eb3dda031f210"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
