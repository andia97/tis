import { Component, OnInit } from '@angular/core';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';
import { PuenteService } from 'src/app/services/puente.service';

@Component({
  selector: 'app-listconvocatory',
  templateUrl: './listconvocatory.page.html',
  styleUrls: ['./listconvocatory.page.scss'],
})
export class ListconvocatoryPage implements OnInit {
  convocatories:any
  messageListEmpty:string
  idConvocatory:any
  constructor(private listConvocatorias:ListConvocatorysService,
              private puenteService:PuenteService) {
                
               }

  ngOnInit() {
    return this.listConvocatorias.getList().subscribe(
      res=>{
        console.log(res);
        
        if(res != "Empty"){
          this.convocatories = res;
        }else{
          this.messageListEmpty ="No existen convocatorias vigentes"
        }
      },
      error=> console.log(error)
    );
  }
  sendId(dataConvocatory:any){
    this.idConvocatory = dataConvocatory
    this.puenteService.sendObjectSource(this.idConvocatory)
    console.log(this.idConvocatory)
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      event.target.complete();
    }, 2000);
  }
}

