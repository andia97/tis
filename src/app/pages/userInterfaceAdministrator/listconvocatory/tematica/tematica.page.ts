import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TematicaService } from 'src/app/services/tematica.service';
import { ListitemsService } from 'src/app/services/listitems.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tematica',
  templateUrl: './tematica.page.html',
  styleUrls: ['./tematica.page.scss'],
})
export class TematicaPage implements OnInit {
idItem:any;

listaDeTematicas:any;
datosDeItem:any;
tematicaForm:FormGroup;
idTematicaForm:FormGroup;

btnDisabled:Boolean;
messageAlert:any;
sumaTotal:any;
get name() {
  return this.tematicaForm.get('name')
}
get portion() {
  return this.tematicaForm.get('portion')
}
public errorsMessages = {
  name: [
    { type: 'required', message: 'Nombre es requerido' },
    { type: 'maxlength', message: 'Maximo 200 caracteres' },
    { type: 'minlength', message: 'Minimo 3 caracteres' },
    { type: 'pattern', message: 'Solo caracteres alfabeticos' }
  ],
  portion: [
    { type: 'required', message: 'Porcentaje es requerido' },
    { type: 'max', message: 'Maximo 3 caracteres numericos' },
    { type: 'pattern', message: 'Solo se permite caracteres numericos' }
  ]
} 

  constructor(private puenteServicio:PuenteService,
              private formBuilder:FormBuilder,
              private tematicaService:TematicaService,
              private itemService:ListitemsService,
              private router:Router,
              private alertCtrl:AlertController) { 
    this.buildTematicaForm();
    this.buildIdTematicaForm();
    this.puenteServicio.$getObjectSource.subscribe((data:any)=>{
      this.idItem=data;
      console.log(this.idItem);
    this.tematicaForm.controls['idItem'].setValue(this.idItem);
    }).unsubscribe();
    this.obtenerItem();
  }

  ngOnInit() {
    this.listarTematicas();
    this.obtenerSumaTotalTematica();
  }
  private buildTematicaForm() {
    this.tematicaForm = this.formBuilder.group({
      idItem: ['',[Validators.required]],
      name: ['', [Validators.required,Validators.maxLength(200), Validators.minLength(3), Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1,0-9-() ]+$')]],
      portion:['',[Validators.required,Validators.max(999)]]
    })
  }
  private buildIdTematicaForm() {
    this.idTematicaForm = this.formBuilder.group({
      id: ['',[Validators.required]]
    })  
  }
  obtenerSumaTotalTematica(){
    this.tematicaService.sumaTotalTematica(this.idItem).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.sumaTotal = res;
          console.log(this.sumaTotal);
        }
      },error=> console.log(error)
      
    );
  }
  obtenerItem(){
    return this.itemService.getItem(this.idItem).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        
        }else{
          this.datosDeItem = res;
          console.log(this.datosDeItem);
         
        }
      },error=>console.log(error)
    );
  }
  listarTematicas() {
    return this.tematicaService.listarTematicas(this.idItem).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
          this.btnDisabled = true;
        }else{
          this.listaDeTematicas = res;
          console.log(this.listaDeTematicas);
          this.btnDisabled = false;
        }
      },error=>console.log(error)
    );
  }
  submit(event:Event){
    console.log(this.tematicaForm.value);

    return this.tematicaService.añadirTematica(this.tematicaForm.value).subscribe(
      res=>{
        if(res == "Succes"){
          console.log(res);
          this.messageAlert = "Tematica añadida correctamente"
          this.alert()
          this.tematicaForm.controls['name'].reset();
          this.tematicaForm.controls['portion'].reset();
          this.doRefresh() 
        }else{
          this.messageAlert = "La suma total del porcentaje de la tematica debe ser igual a 100%"
          this.alert()
        }
      },error => console.log(error)
    );
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }
  async alert(){
    const alert = await this.alertCtrl.create({
      message: this.messageAlert,
      buttons: ['OK']
    });
    await alert.present();
  }
  eliminarTematica(id){
    console.log();
    this.idTematicaForm.controls['id'].setValue(id);
    return this.tematicaService.eliminarTematica(this.idTematicaForm.value).subscribe(
      res=>{
        console.log(res);
        this.doRefresh();
      },error=>console.log(error)
    );
  }
  goToHome(){
    this.router.navigate(['/home/listconvocatoryAdmin/item'])
  }
}
