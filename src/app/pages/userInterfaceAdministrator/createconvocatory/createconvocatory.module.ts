import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateconvocatoryPageRoutingModule } from './createconvocatory-routing.module';

import { CreateconvocatoryPage } from './createconvocatory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateconvocatoryPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateconvocatoryPage]
})
export class CreateconvocatoryPageModule {}
