import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReqoptionsPage } from './reqoptions.page';

describe('ReqoptionsPage', () => {
  let component: ReqoptionsPage;
  let fixture: ComponentFixture<ReqoptionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqoptionsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReqoptionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
