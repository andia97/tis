import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfMeritosPage } from './conf-meritos.page';

const routes: Routes = [
  {
    path: '',
    component: ConfMeritosPage
  },
  {
    path:'reqoptions',
    loadChildren: () => import('../reqoptions/reqoptions.module').then( m => m.ReqoptionsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfMeritosPageRoutingModule {}
