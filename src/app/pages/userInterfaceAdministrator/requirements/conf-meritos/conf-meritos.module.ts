import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfMeritosPageRoutingModule } from './conf-meritos-routing.module';

import { ConfMeritosPage } from './conf-meritos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfMeritosPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ConfMeritosPage]
})
export class ConfMeritosPageModule {}
