import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequirementsPage } from './requirements.page';

const routes: Routes = [
  {
    path: '',
    component: RequirementsPage
  },
  {
    path:'reqoptions',
    loadChildren: () => import('./reqoptions/reqoptions.module').then( m => m.ReqoptionsPageModule)
  },
  {
    path: 'conf-meritos',
    loadChildren: () => import('./conf-meritos/conf-meritos.module').then( m => m.ConfMeritosPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequirementsPageRoutingModule {}
