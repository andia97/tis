import { Component, OnInit } from '@angular/core';
import { ListreqService } from 'src/app/services/listreq.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PuenteService } from 'src/app/services/puente.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-requirements',
  templateUrl: './requirements.page.html',
  styleUrls: ['./requirements.page.scss'],
})
export class RequirementsPage implements OnInit {

  listRequisitos: any;
  listAddRequisitos:any;
  listAddRequisitosExtra:any;

  requirementForm: FormGroup;
  deleteForm:FormGroup;
  requirementExtraForm: FormGroup; 

  idConv:any;
  idReq:any;
  messgeAlert:string;
  estado:String = "false";
  get reqExtra() {
    return this.requirementExtraForm.get('name_extra')
  }
  public errorsMessages = {
    reqExtra: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'maxlength', message: 'Maximo 100 caracteres alfabeticos' },
      { type: 'minlength', message: 'Minimo 3 caracteres alfabeticos' },
      { type: 'pattern', message: 'Solo caracteres alfabeticos' }
    ]
  }
  constructor(private listReq: ListreqService,
              private formBuilder: FormBuilder,
              private puenteServicio: PuenteService,
              private alertCtrl:AlertController,
              private router:Router) { 
    this.buildForm()
    this.buildFormDelete()
    this.buildFormAddExtra()
    this.puenteServicio.$getObjectSource.subscribe((data:any)=>{
      this.idConv = data
      console.log(this.idConv)
      this.requirementForm.controls['idConv'].setValue(this.idConv)
      this.requirementExtraForm.controls['id_conv'].setValue(this.idConv)
      this.requirementForm.controls['estado'].setValue(this.estado)
      this.requirementExtraForm.controls['estado'].setValue(this.estado)
    }).unsubscribe();
  }

  ngOnInit() {
    this.listarRequerimientos()
    this.listarRequerimientosExtra()
    //console.log(this.requirementForm.value);
    this.listarRequerimientosAñadidos()
    
  }
  private buildFormAddExtra() {
    this.requirementExtraForm = this. formBuilder.group({
      id_conv:[""],
      name_extra:["",[Validators.required,Validators.maxLength(100), Validators.minLength(3), Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$')]],
      estado:['']
    })
  }
  private buildFormDelete() {
    this.deleteForm = this.formBuilder.group({
      id:['',[Validators.required]]
    })
  }
  private buildForm() {
    this.requirementForm = this.formBuilder.group({
      idConv:['',  [Validators.required]],
      idReq:['',  [Validators.required]],
      estado:['']
    })
  }
  listarRequerimientosExtra(){
    return this.listReq.getReqByConvExtra(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listAddRequisitosExtra = res;
        }
      },
      error => console.log(error)
    )
  }
  listarRequerimientosAñadidos(){
    return this.listReq.getReqByConv(this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listAddRequisitos = res;
        }
      },
      error => console.log(error)
    )
  }
  listarRequerimientos(){
    return this.listReq.getList().subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);          
        }else{
          this.listRequisitos = res;
        }
      },error=> console.log(error)
    )
  }
  aniadir(requisitoId:any){
    this.idReq = requisitoId
    this.requirementForm.controls['idReq'].setValue(this.idReq)
    console.log(this.requirementForm.value);
    this.añadirRequisito()
  }
  private añadirRequisito() {
    return this.listReq.addReq(this.requirementForm.value).subscribe(
      res=>{
        if(res == "1"){
          this.messgeAlert = "Ya añadio este requisito"
          this.alert();
        }else{
          this.messgeAlert = "Añadido"
          this.alert();
          this.doRefresh();
          //window.location.reload()
        }
      },
      error=> console.log(error)
    )
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }
  delReq(id){
    this.deleteForm.controls['id'].setValue(id);
    console.log(this.deleteForm.value);
    return this.listReq.deleteReq(this.deleteForm.value).subscribe(
      res=>{
        console.log(res);
        this.doRefresh();
      },
      error=>console.log(error)
    );
  }
  sendIdConv(){
    this.puenteServicio.sendObjectSource(this.idConv)
    console.log(this.idConv)
  }

  submit(){
    console.log("Se añadio el extra");
    console.log(this.requirementExtraForm.value);
    return this.listReq.addReqExtra(this.requirementExtraForm.value).subscribe(
      res=>{
        console.log(res);
          this.requirementExtraForm.controls["name_extra"].reset()
          this.messgeAlert = "Añadido"
          this.alert();
          this.doRefresh();
      },
      error=>console.log(error)
    )
  }
  goToHome(){
    this.router.navigate(['/home/listconvocatoryAdmin'])
  }
}
