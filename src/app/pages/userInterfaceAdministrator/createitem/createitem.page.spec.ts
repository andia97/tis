import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateitemPage } from './createitem.page';

describe('CreateitemPage', () => {
  let component: CreateitemPage;
  let fixture: ComponentFixture<CreateitemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateitemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateitemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
