import { Component, OnInit, Input } from '@angular/core';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';
import { ComisionService } from 'src/app/services/comision.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PuenteService } from 'src/app/services/puente.service';
import { ModalController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-modal-asignacion',
  templateUrl: './modal-asignacion.page.html',
  styleUrls: ['./modal-asignacion.page.scss'],
})
export class ModalAsignacionPage implements OnInit {

  idUser:any;
  listaConvocatorias:any;
  listaRoles:any;

  rolForm:FormGroup;

  constructor(private convocatorias: ListConvocatorysService,
              private roles:ComisionService,
              private formBuilder:FormBuilder,
              private puenteServicio:PuenteService,
              private modalCtrl:ModalController,
              private alertCtrl:AlertController) {
                this.buildForm();
                this.puenteServicio.$getObjectSource.subscribe((data:any)=>{
                  this.idUser = data
                  console.log(this.idUser)
                  this.rolForm.controls['idUser'].setValue(this.idUser)
                }).unsubscribe();              
              }

  ngOnInit() {
    this.listarConvocatorias();
    this.listarRoles();
  }
  private buildForm() {
    this.rolForm = this.formBuilder.group({
      idUser:[''],
      idConv:['',[Validators.required]],
      idRol:['',[Validators.required]]
    })
  }
  listarConvocatorias(){
    return this.convocatorias.getList().subscribe(
      res=>{
        if(res=="empty"){
          console.log(res); 
        }else{
          this.listaConvocatorias = res;
          console.log(this.listaConvocatorias);
        }

      }, error=>console.log(error)
    );
  }
  listarRoles(){
    return this.roles.listarRoles().subscribe(
      res=>{
        if(res=="Empty"){
          console.log(res);
        }else{
          this.listaRoles = res;
          console.log(this.listaRoles);
        }
      },error=>console.log(error)
    );
  }
  submit(event:Event){
    console.log(this.rolForm.value);
    return this.roles.registrarRolUsuario(this.rolForm.value).subscribe(
      res=>{
        if(res =="Succes"){
          console.log(res);
          this.modalCtrl.dismiss();
        }else{
          this.alert();
        }
      }, error=>console.log(error)
      
    );
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: 'Usuario ya registrado, prueba con otro',
      buttons: ['OK']
    });

    await alert.present();
  }
}
