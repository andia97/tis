import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalAsignacionPageRoutingModule } from './modal-asignacion-routing.module';

import { ModalAsignacionPage } from './modal-asignacion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalAsignacionPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ModalAsignacionPage]
})
export class ModalAsignacionPageModule {}
