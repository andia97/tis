import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalAsignacionPage } from './modal-asignacion.page';

const routes: Routes = [
  {
    path: '',
    component: ModalAsignacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalAsignacionPageRoutingModule {}
