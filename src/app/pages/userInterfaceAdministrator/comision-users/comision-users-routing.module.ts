import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComisionUsersPage } from './comision-users.page';

const routes: Routes = [
  {
    path: '',
    component: ComisionUsersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComisionUsersPageRoutingModule {}
