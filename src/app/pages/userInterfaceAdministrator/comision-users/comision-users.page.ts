import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterUserService } from 'src/app/services/register-user.service';
import { UsersService } from 'src/app/services/users.service';
import { AlertController, ModalController } from '@ionic/angular';
import { ModalAsignacionPage } from '../modal-asignacion/modal-asignacion.page';
import { PuenteService } from 'src/app/services/puente.service';

@Component({
  selector: 'app-comision-users',
  templateUrl: './comision-users.page.html',
  styleUrls: ['./comision-users.page.scss'],
})
export class ComisionUsersPage implements OnInit {

  userComisionList:any;

  comisionForm:FormGroup;

  get name() {
    return this.comisionForm.get('firstName')
  }
  get userName() {
    return this.comisionForm.get('userName')
  }
  get password() {
    return this.comisionForm.get('password')
  }
  get passwordConfirm() {
    return this.comisionForm.get('passwordConfirm')
  }

  public errorsMessages = {    
    name: [
      { type: 'required', message: 'Nombre(s) es requerido' },
      { type: 'maxlength', message: 'Máximo 35 caracteres' },
      { type: 'minlength', message: 'Mínimo 3 caracteres alfabéticos' },
      { type: 'pattern', message: 'Solo se permiten caracteres' }
    ],
    userName: [
      { type: 'required', message: 'Nombre Usuario es requerido' },
      { type: 'maxlength', message: 'Máximo 35 caracteres' },
      { type: 'minlength', message: 'Mínimo 3 caracteres alfabetico' }
    ],
    password: [
      { type: 'required', message: 'Contraseña es requerido' },
      { type: 'maxlength', message: 'Máximo 8 caracteres' },
      { type: 'minlength', message: 'Mínimo 8 caracteres' },
    ],
    passwordConfirm: [
      { type: 'required', message: 'Contraseña es requerido' }
    ]
  }
  checkPasswords(group: FormGroup) {
    let contraseña = group.controls.password.value
    let confirmarContraseña = group.controls.passwordConfirm.value

    return contraseña === confirmarContraseña ? null : { notSame: true }
  }
  constructor(private formBuilder:FormBuilder,
              private userRegister:RegisterUserService,
              private userList:UsersService,
              private alertCtrl:AlertController,
              private modalController:ModalController,
              private puenteServicio:PuenteService) {

    this.buildForm();

  }

  ngOnInit() {
    this.listarUsuariosComision();
  }
  listarUsuariosComision(){
    return this.userList.getUserComision().subscribe(
      res=>{
        if(res=="Empty"){
          console.log(res);
        }else{
          this.userComisionList = res;
          console.log(this.userComisionList);
          
        }
      },error=>console.log(error)
    );
  }
  private buildForm() {
    this.comisionForm = this.formBuilder.group({
      firstName:['',[Validators.required, Validators.maxLength(35), Validators.minLength(3), Validators.pattern('^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$')]],
      userName:['',[Validators.required, Validators.maxLength(38), Validators.minLength(3)]],
      password:['',[Validators.required, Validators.maxLength(8), Validators.minLength(8)]],
      passwordConfirm:['',[Validators.required]],
      tipo:[''],
      email:['']
    },{ validator: this.checkPasswords }
    )
  }

  submit(){
    this.comisionForm.controls['tipo'].setValue('Comision');
    return this.userRegister.registerUser(this.comisionForm.value).subscribe(
      res=>{
        console.log(res);
        this.alert();
        this.doRefresh();
        this.comisionForm.reset();
      },error=>console.log(error)
    );
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: 'Registrado Correctamete',
      buttons: ['OK']
    });

    await alert.present();
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalAsignacionPage,
      componentProps: {}
    });
    return await modal.present();
  }
  sendIdUser(idUser){
    console.log(idUser);
    this.puenteServicio.sendObjectSource(idUser);
  }
}
