import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConvocatoriaPageRoutingModule } from './convocatoria-routing.module';

import { ConvocatoriaPage } from './convocatoria.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConvocatoriaPageRoutingModule,
    ComponentsModule
  ],
  declarations: [ConvocatoriaPage]
})
export class ConvocatoriaPageModule {}
