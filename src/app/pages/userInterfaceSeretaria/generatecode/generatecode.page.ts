import { Component, OnInit } from '@angular/core';
import { CodigoService } from 'src/app/services/codigo.service';
import { FormControl, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ToastService } from 'src/app/services/toast.service.js';
import emailjs, { EmailJSResponseStatus } from 'emailjs-com';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-generatecode',
  templateUrl: './generatecode.page.html',
  styleUrls: ['./generatecode.page.scss'],
})
export class GeneratecodePage implements OnInit {

  codigo: any;
  messgeAlert:any;
  
  tipoDeUsuario: string;

  emailCtrl = new FormControl('', [Validators.required,Validators.email]);

  constructor(
    private codigoService: CodigoService,
    private alertCtrl:AlertController
  ) {
    this.emailCtrl.valueChanges.pipe(debounceTime(1000)).subscribe(value => {
      console.log(value);
    });

  }


  ngOnInit() {
  }
  pedirCodigo(tipoDeUsuario: string) {
    this.codigoService.getCodigo(tipoDeUsuario).subscribe(res => {
      this.codigo = res;
      if (this.emailCtrl.value) {

        var templateParams = {
          subject: "Plataforma de gestión de convocatorias",
          toEmail: this.emailCtrl.value,
          codigo: this.codigo
        };

        emailjs.send('gmail', 'template_zUUBDCsD', templateParams, 'user_C5QNKtJIqkRuXBdySW9rt').then((result: EmailJSResponseStatus) => {
          //this.toastService.presentToast("Se envió el código de acceso al correo electrónico.", 4000);
          this.messgeAlert = "Se envió el código de acceso al correo electrónico: " + this.emailCtrl.value;
          this.alert();
          this.emailCtrl.setValue("");
          this.codigo = null;
          console.log(result.text);
        }, (error) => {
          console.log(error.text);
        });
      }
    });
  }

  limpiar() {
    this.codigo = null;
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
}
