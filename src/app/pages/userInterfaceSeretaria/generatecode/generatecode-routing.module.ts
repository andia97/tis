import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeneratecodePage } from './generatecode.page';

const routes: Routes = [
  {
    path: '',
    component: GeneratecodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneratecodePageRoutingModule {}
