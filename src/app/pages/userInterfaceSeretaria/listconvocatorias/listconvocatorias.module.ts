import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListconvocatoriasPageRoutingModule } from './listconvocatorias-routing.module';

import { ListconvocatoriasPage } from './listconvocatorias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListconvocatoriasPageRoutingModule
  ],
  declarations: [ListconvocatoriasPage]
})
export class ListconvocatoriasPageModule {}
