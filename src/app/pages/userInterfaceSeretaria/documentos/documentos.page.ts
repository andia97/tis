import { Component, OnInit } from '@angular/core';
import { ListreqService } from 'src/app/services/listreq.service';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { PuenteService } from 'src/app/services/puente.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.page.html',
  styleUrls: ['./documentos.page.scss'],
})
export class DocumentosPage implements OnInit {
  listaDeRequisitos:any;
  listaDeRequisitosExtra:any;
  idConv:any;
  idInscripcion:any;
  idUser:String;
  nombre:any;
  estado:String;
  validarForm:FormGroup;
  validarDocumentoForm:FormGroup;
  messageAlert:string;
  files = [];

  fileState = new Array<String>();

  disabledBtn:Boolean = true;
  constructor(private requisitos:ListreqService,
              private storageService:StorageService,
              private puenteService:PuenteService,
              private formBuilder:FormBuilder,
              private router:Router,
              private alertCtrl:AlertController){
                this.buildFormValidar();
                this.buildValidarDocumentoForm();
                this.puenteService.$getObjectSource.subscribe((data:any)=>{
                  this.idUser = data.userId;
                  this.idInscripcion = data.inscId;
                  this.validarForm.controls['idUser'].setValue(this.idUser);
                  this.nombre = data.first_name+''+data.last_namep+''+data.last_namem;
                }).unsubscribe();
              }
  ngOnInit() {
    this.listarRequisitosObligtorios();
    //this.listarRequisitosExtras();
    this.requisitos.listarDocumentosPresentados(this.idInscripcion).subscribe(
      res=>{
        this.files = res;
        this.createStateArray(this.files);
        this.disabledBtn = this.validFileState(this.fileState)
      }
    );
  }
  private buildFormValidar() {
    this.validarForm = this.formBuilder.group({
      idUser:[''],
      idConv:[''],
      estado:['']
    });
  }
  private buildValidarDocumentoForm() {
    this.validarDocumentoForm = this.formBuilder.group({
      idFile:[''],
      estado:['']
    });
  }
  listarRequisitosObligtorios(){
      this.requisitos.listarDocumentosPresentados(this.idInscripcion).subscribe(
        res=>{
          if(res == "Empty"){
            console.log(res);
          }else{
            this.listaDeRequisitos = res;
            console.log(this.listaDeRequisitos); 
          }
        },error=>console.log(error)
      );
  }

  createStateArray(files:any){
    files.forEach( (file:any) => {
      this.fileState.push(file.estado);
    });
    console.log(this.fileState);
    
  }

  updateEstado(idFile:any,estado:any) {
    console.log('Nuevo estado:' + estado +' Con el idFile:' + idFile);
    this.validarDocumentoForm.controls['idFile'].setValue(idFile);    
    if(estado == "false"){
      let estadoTrue = "true"
      this.validarDocumentoForm.controls['estado'].setValue(estadoTrue);
      this.requisitos.validarDocumento(this.validarDocumentoForm.value).subscribe(
            res=>{
              if(res == "Succes"){
                console.log(res);
                this.fileState = new Array();
                this.doRefresh();
              }else{
                console.log(res);
              }
            }, error=>console.log(error)
          );
    }else{
      let estadoFalse = "false"
      this.validarDocumentoForm.controls['estado'].setValue(estadoFalse);
      this.messageAlert="Ya no puede validar el documento. El documento ya se validó anteriormente"
      this.alert();
      this.doRefresh();
    }
    console.log(this.validarDocumentoForm.value);
    
  }

  inhabilitarPostulante(){
    this.estado="false"
    this.validarForm.controls['estado'].setValue(this.estado);
    this.storageService.get(AuthConstants.ID_CONV).then(
      res=>{
        this.idConv=res;
        this.validarForm.controls['idConv'].setValue(this.idConv)
        console.log(this.validarForm.value);
        
        return this.requisitos.habilitarPostulante(this.validarForm.value).subscribe(
          res=>{
            console.log(res);
            if( res == "Succes"){
              this.messageAlert="Postulante inhabilitado"
              this.alert();
              this.router.navigate(['/home/listconvocatorias/listusers']);
            }else{
              this.messageAlert="Postulante ya evaluado, prueba con otro"
              this.alert();
            }
          }
        );
      }
    );

  }
  habilitarPostulante(){
      this.estado="true"
      this.validarForm.controls['estado'].setValue(this.estado);
      this.storageService.get(AuthConstants.ID_CONV).then(
        res=>{
          this.idConv=res;
          this.validarForm.controls['idConv'].setValue(this.idConv)
          console.log(this.validarForm.value);
          
          this.requisitos.habilitarPostulante(this.validarForm.value).subscribe(
            res=>{
              console.log(res);
              if( res == "Succes"){
                this.messageAlert="Postulante habilitado"
                this.alert();
                this.router.navigate(['/home/listconvocatorias/listusers']);
              }else{
                this.messageAlert="Postulante ya esta evaluado, prueba con otro"
                this.alert();
              }
            }
          );
        }
      );
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messageAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }

  validFileState(files:Array<any>):Boolean{
    console.log(files);
    const array = Array.from(files)
    let count = array.length;
    console.log(count);
    let res:Boolean=false;
    for( let i = 0; i<count; i=i+1){
      if(array[i] != "true"){
        console.log("Los elementos en cada posicion son:");
        console.log(array[i]);
        res = true;
      }else{
        console.log("Los elementos en cada posicion son:");
        console.log(array[i]);
      }
    }
    console.log(res);
    
    return res;
  } 
}
