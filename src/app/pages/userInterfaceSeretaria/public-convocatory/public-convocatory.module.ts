import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicConvocatoryPageRoutingModule } from './public-convocatory-routing.module';

import { PublicConvocatoryPage } from './public-convocatory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicConvocatoryPageRoutingModule
  ],
  declarations: [PublicConvocatoryPage]
})
export class PublicConvocatoryPageModule {}
