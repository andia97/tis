import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PublicConvocatoryPage } from './public-convocatory.page';

describe('PublicConvocatoryPage', () => {
  let component: PublicConvocatoryPage;
  let fixture: ComponentFixture<PublicConvocatoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicConvocatoryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PublicConvocatoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
