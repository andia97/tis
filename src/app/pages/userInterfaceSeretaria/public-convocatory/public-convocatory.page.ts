import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalUsuariosPage } from '../../modal-usuarios/modal-usuarios.page';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';

@Component({
  selector: 'app-public-convocatory',
  templateUrl: './public-convocatory.page.html',
  styleUrls: ['./public-convocatory.page.scss'],
})
export class PublicConvocatoryPage implements OnInit {

  listaDeConvoctorias:Array<any>;

  constructor(private modalCtrl: ModalController,
              private convocatoria:ListConvocatorysService) { }

  ngOnInit() {
    this.listarConvocatorias();
  }
  listarConvocatorias(){
    return this.convocatoria.getList().subscribe(
      res=>{
        if(res == "Emty"){
          console.log(res);
        }else{
          this.listaDeConvoctorias = res;
        }
      }
    );
  }
  async presentModal(idConv,condicion) {
    const modal = await this.modalCtrl.create({
      component: ModalUsuariosPage,
      componentProps: {
        'condicion':condicion,
        'idConvocatoria':idConv
      }
    });
    return await modal.present();
  }
}
