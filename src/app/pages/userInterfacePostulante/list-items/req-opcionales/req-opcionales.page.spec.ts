import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReqOpcionalesPage } from './req-opcionales.page';

describe('ReqOpcionalesPage', () => {
  let component: ReqOpcionalesPage;
  let fixture: ComponentFixture<ReqOpcionalesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqOpcionalesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReqOpcionalesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
