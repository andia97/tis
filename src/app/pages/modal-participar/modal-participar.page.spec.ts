import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalParticiparPage } from './modal-participar.page';

describe('ModalParticiparPage', () => {
  let component: ModalParticiparPage;
  let fixture: ComponentFixture<ModalParticiparPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalParticiparPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalParticiparPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
