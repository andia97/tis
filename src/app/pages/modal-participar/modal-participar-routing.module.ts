import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalParticiparPage } from './modal-participar.page';

const routes: Routes = [
  {
    path: '',
    component: ModalParticiparPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalParticiparPageRoutingModule { }
