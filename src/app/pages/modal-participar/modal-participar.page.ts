import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from '@angular/forms';
import { GetObligatoriosService } from '../../services/get-obligatorios.service';
import { ModalController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { Observable } from 'rxjs';
import { UploadTaskSnapshot } from '@angular/fire/storage/interfaces';
import { InscriptionconvocatoyService } from 'src/app/services/inscriptionconvocatoy.service';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { ListinscriptionconvocatoryService } from 'src/app/services/listinscriptionconvocatory.service';

@Component({
  selector: 'app-modal-participar',
  templateUrl: './modal-participar.page.html',
  styleUrls: ['./modal-participar.page.scss'],
})
export class ModalParticiparPage implements OnInit {

  @Input() idConvocatoria: string;
  inscriptionConvocatoryForm: FormGroup;
  sendData: FormGroup;

  formulario: FormGroup;
  shortnamesArray = new Array();
  failFormat = new Array();

  files = new Array<File>();
  downloadUrl$: Observable<string>;
  uploadPercent$: Observable<number>;
  idUser: string;

  items: [];

  postData = {
    idConvocatoria: ''
  }

  messgeAlert: any;

  constructor(
    private formBuilder: FormBuilder,
    private getObligatoriosService: GetObligatoriosService,
    private modalController: ModalController,
    private storage: AngularFireStorage,
    private storageService: StorageService,
    private inscriptionConvocatory: InscriptionconvocatoyService,
    private alertCtrl: AlertController,
    private uploadService: FileUploadService,
    private listInscription: ListinscriptionconvocatoryService,
    private router: Router
  ) {
    this.buildForm();
  }

  ngOnInit() {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.idUser = res;
    });

    this.postData.idConvocatoria = this.idConvocatoria;
    this.getObligatoriosService.getObligatorios(this.postData).subscribe(res => {
      this.items = res;
      this.createArrayShortnames(this.items);
    });

  }
  ionViewWillEnter() {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.idUser = res;
      console.log(this.idUser);
      this.inscriptionConvocatoryForm.controls['idUser'].setValue(this.idUser);
      this.inscriptionConvocatoryForm.controls['idConv'].setValue(this.idConvocatoria);
    });
  }

  private buildForm() {
    this.inscriptionConvocatoryForm = this.formBuilder.group({
      idUser: ['', [Validators.required]],
      idConv: ['', [Validators.required]]
    });

    this.sendData = this.formBuilder.group({
      id_insc_conv: ['', [Validators.required]],
      nameReq: ['', [Validators.required]],
      file_dir: ['', [Validators.required]],
      tipo: ['obligatorio', [Validators.required]],
      estado: ['false', []],
      observacion: ['', []],
      puntuacion: ['', []]
    });
  }

  createArrayShortnames(items: any) {
    items.forEach((item: any) => {
      this.shortnamesArray.push(item.nombre);
    });
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  irAOpcionales() {
    console.log(this.inscriptionConvocatoryForm.value);
    this.uploadService.getIdInscConv(this.inscriptionConvocatoryForm.value).subscribe(res => {
      this.sendData.controls['id_insc_conv'].setValue(res[0].id);
    }, error => console.log(error)
    );
    this.enviar();
    this.messgeAlert = "Participación con éxito"
    this.alert();
    /*this.inscriptionConvocatory.inscribirPostulante(this.inscriptionConvocatoryForm.value).subscribe(res => {
      console.log(res);
      if (res == "Succes") {

        //this.doRefresh(event);

      } else {
        this.messgeAlert = "No se pudo completar la inscripción, intente nuevamente"
        this.alert();
        //this.inscriptionConvocatoryForm.reset();
        this.doRefresh(event);
      }
    }, error => console.log(error)
    );*/
    this.dismiss();
  }

  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }

  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 3000);
  }


  checkFile(e, index) {
    if (e.target.files[0].type == "application/pdf") {
      this.failFormat[index] = false;
      this.files[index] = e.target.files[0];
    } else {
      this.failFormat[index] = true;
    }
    console.log(this.failFormat[index]);
    console.log(this.files);
  }

  enviar() {
    for (let i = 0; i < this.files.length; i++) {
      let file = this.files[i];
      let path = 'files/' + this.shortnamesArray[i] + '-User' + this.idUser + '-Conv' + this.idConvocatoria;
      let uploadTask: AngularFireUploadTask = this.storage.upload(path, file);

      this.uploadPercent$ = uploadTask.percentageChanges();
      uploadTask.then((uploadSnapshot: UploadTaskSnapshot) => {
        uploadSnapshot.ref.getDownloadURL().then((downloadURL) => {
          //Aqui se obtiene las urls de descarga de los archivos subidos, estas url ahoras hay que almacenarlas en DB para despues usarlas
          this.downloadUrl$ = downloadURL;
          this.sendData.controls['file_dir'].setValue(this.downloadUrl$);
          this.sendData.controls['nameReq'].setValue(this.shortnamesArray[i]);
          this.uploadService.setFileDir(this.sendData.value).subscribe(res => {
            console.log(res);
          });
        })
      });
    }
  }
  deleteInscription() {
    console.log(this.inscriptionConvocatoryForm.value);
    return this.listInscription.deleteInscription(this.inscriptionConvocatoryForm.value).subscribe(
      res => {
        console.log(res);
        this.confirmar();
        this.messgeAlert = "Postulación Incompleta";
        this.alert();
        this.dismiss();
      }
    );

  }
  confirmar() {
    console.log('Begin async operation');
    setTimeout(() => {
      this.ngOnInit()
      window.location.reload();
    }, 3000);
  }
}
