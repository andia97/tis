import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { IonicModule } from '@ionic/angular';

import { ModalParticiparPageRoutingModule } from './modal-participar-routing.module';

import { ModalParticiparPage } from './modal-participar.page';

import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    IonicModule,
    ModalParticiparPageRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatButtonModule
  ],
  declarations: [ModalParticiparPage]
})
export class ModalParticiparPageModule { }
