import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Usuario } from 'src/app/models/usuario/usuario';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { PuenteService } from 'src/app/services/puente.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  messageError: String = ""
  validUser: Boolean = false;

  get userName() {
    return this.loginForm.get('userName')
  }
  get password() {
    return this.loginForm.get('password')
  }

  public errorsMessages = {
    userName: [
      { type: 'required', message: 'Nombre es requerido' },
      { type: 'maxlength', message: '15 caracteres como maximo' },
      { type: 'minlength', message: '2 caracteres como minimo' }
    ],
    password: [
      { type: 'required', message: 'Password es requerido' },
      { type: 'maxlength', message: '8 caracteres como maximo' },
      { type: 'minlength', message: '6 caracteres como minimo' }
    ]
  }


  loginForm = this.formBuilder.group({
    userName: ['', [Validators.required, Validators.maxLength(15), Validators.minLength(2)]],
    password: ['', [Validators.required, Validators.maxLength(8), Validators.minLength(6)]]
  })
  hidden = false
  datosUsuario: any;
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private authService: AuthService,
    private storageService: StorageService,
    private puenteService:PuenteService
  ) {

  }
  public submit() {
    this.authService.authUser(this.loginForm.value)
      .subscribe(
        res => {
          //this.datosUsuario = res;
          //console.log(res[0])

          if (res == "2" || res == "3") {
            if (res[0] == "2") {
              this.messageError = "Contraseña invalida"
              this.validUser = true;
            }
            if (res[0] == "3") {
              this.messageError = "Usuario no registrado"
              this.validUser = true;
            }

          } else {
            this.datosUsuario = res;
            console.log(this.datosUsuario);
            console.log(this.datosUsuario[0].id);
            this.storageService.store(AuthConstants.AUTH, this.datosUsuario[0].id);
            this.storageService.store(AuthConstants.TYPE_USER, this.datosUsuario[0].tipo);
            this.router.navigate(['/home']);
          }
        },
        err => console.error(err)
      );
    this.loginForm.reset();
    this.validUser = false;
  }

  ngOnInit() {
  }

}
