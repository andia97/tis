import { Component, OnInit } from '@angular/core';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { PuenteService } from 'src/app/services/puente.service';

@Component({
  selector: 'app-convocatorias',
  templateUrl: './convocatorias.page.html',
  styleUrls: ['./convocatorias.page.scss'],
})
export class ConvocatoriasPage implements OnInit {
  idUser:any;
  listaDeConvocatorias:any;
  constructor(private convocatorias:ListConvocatorysService,
              private storageService:StorageService,
              private puenteService:PuenteService) { }

  ngOnInit() { 
    this.listarConvocatorias();
  }
  ionViewWillEnter() {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      this.idUser = res;
      console.log(this.idUser);
    });
  }
  listarConvocatorias(){
    this.storageService.get(AuthConstants.AUTH).then(res => {
    this.idUser = res;
    return this.convocatorias.listarConvocatoriasComision(this.idUser).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.listaDeConvocatorias = res;
          console.log(this.listaDeConvocatorias);
        }
      },error => console.log(error)
    );
  });
  }
  sendIdConv(conv){
    this.puenteService.sendObjectSource(conv);
  }
}
