import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeritosPage } from './meritos.page';

const routes: Routes = [
  {
    path: '',
    component: MeritosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeritosPageRoutingModule {}
