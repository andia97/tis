import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MeritosPage } from './meritos.page';

describe('MeritosPage', () => {
  let component: MeritosPage;
  let fixture: ComponentFixture<MeritosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeritosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MeritosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
