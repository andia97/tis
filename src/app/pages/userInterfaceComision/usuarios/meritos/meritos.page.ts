import { Component, OnInit } from '@angular/core';
import { PuenteService } from 'src/app/services/puente.service';
import { ReqOptionService } from 'src/app/services/req-option.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalMeritosPage } from 'src/app/pages/modal-meritos/modal-meritos.page';
import { ListreqService } from 'src/app/services/listreq.service';

@Component({
  selector: 'app-meritos',
  templateUrl: './meritos.page.html',
  styleUrls: ['./meritos.page.scss'],
})
export class MeritosPage implements OnInit {
  nombre:any;
  idConv:any;
  idUser:any;
  idInscri:any;

  listaDeRendimientos:any;
  listaDeNotas:any;
  listaDeReqOpcionales:any;
  listaDePuntos:any;
  listaObligatorios:any;

  puntosExpercienciaForm:FormGroup;
  notaRendimientoForm:FormGroup;
  idInscriForm:FormGroup;

  messgeAlert:any;
  notaTotalDeUsuario:any;
  puntosTotalDeUsuario:any;
  constructor(private puenteServicio:PuenteService,
              private formuBuilder:FormBuilder,
              private reqOption:ReqOptionService,
              private reqObligatoriosServecice:ListreqService,
              private alertCtrl:AlertController,
              private modalCtrl:ModalController,
              private router:Router) {
      this.buildForm();
      this.buildFormPuntos();
      this.puenteServicio.$getObjectSource.subscribe((data:any)=>{
      this.nombre = data.first_name+' '+data.last_namep+' '+data.last_namem;
      this.idConv = data.id_conv;
      this.idUser = data.id_user;
      //console.log(data);
      this.notaRendimientoForm.controls['idUser'].setValue(this.idUser)
      this.puntosExpercienciaForm.controls['idUser'].setValue(this.idUser)
      this.notaRendimientoForm.controls['idConv'].setValue(this.idConv)
      this.puntosExpercienciaForm.controls['idConv'].setValue(this.idConv)
    }).unsubscribe();
  }

  ngOnInit() {

    this.listarRendimientos();
    this.listarNotasDeUsuario();
    this.obtenerNotaTotal();
    this.listarPuntosDeUsuario();
    this.obtenerPuntoTotal();
    this.listarRequisitosOpcionales();
    this.listarObligatorios();
 }
  private buildForm() {
    this.notaRendimientoForm = this.formuBuilder.group({
      idUser:[''],
      idRend:[''],
      idConv:[''],
      nota:['',[Validators.required,Validators.min(0),Validators.max(100)]]
    });
  }
  private buildFormPuntos() {
    this.puntosExpercienciaForm = this.formuBuilder.group({
      idUser:[''],
      idReq:[''],
      idConv:[''],
      punto:['',[Validators.required]]
    });
  }
  listarObligatorios(){
    this.reqOption.getIdInscri(this.idUser,this.idConv).subscribe(
      data=>{
        console.log(data);
        this.idInscri = data[0].id;
        console.log("El id de inscri es " + this.idInscri );
        this.reqObligatoriosServecice.listarDocumentosPresentados(this.idInscri).subscribe(
          res=>{
            if( res == "Empty"){
              console.log(res);
            }else{
              this.listaObligatorios = res;
              console.log(res);
              
            }
          },error=>console.log(error)
          
          );
        }
    );
  }
  obtenerNotaTotal(){
    return this.reqOption.sumarNotaUsuario(this.idUser,this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
        }else{
          this.notaTotalDeUsuario = res;
        }
      },error=>console.log(error)
    );
  }
  obtenerPuntoTotal(){
    this.reqOption.getIdInscri(this.idUser,this.idConv).subscribe(
      data=>{
        console.log(data);
        this.idInscri = data[0].id;
        console.log("El id de inscri es " + this.idInscri );
        this.reqOption.sumarPuntosUsuario(this.idInscri).subscribe(
          res=>{
            if( res == "Empty"){
              console.log(res);
            }else{
              this.puntosTotalDeUsuario = res[0];
              console.log(res);
              
            }
          },error=>console.log(error)
          
          );
        }
    );
  }
  listarNotasDeUsuario(){
    return this.reqOption.listarNotas(this.idUser,this.idConv).subscribe(
      res=>{
        if(res =="Empty"){
          console.log(res);
          //this.listaDeNotas = this.listaDeRendimientos;
        }else{
          this.listaDeNotas = res;
          console.log(this.listaDeNotas);
        }
      },error=>console.log(error)
      
    );
  }
  listarPuntosDeUsuario(){
    return this.reqOption.listarPuntos(this.idUser,this.idConv).subscribe(
      res=>{
        if(res == "Empty"){
          console.log(res);
          
          //this.listaDePuntos = this.listaDeReqOpcionales;
        }else{
          this.listaDePuntos = res;
          console.log(this.listaDePuntos);
          
        }
      },error=>console.log(error)
    );
  }
  listarRendimientos(){
    return this.reqOption.listarRendimientos(this.idConv).subscribe(
      res=>{
        if(res =="Empty"){
          console.log(res);
        }else{
          this.listaDeRendimientos = res;
          console.log(this.listaDeRendimientos);
          
        }
      },error=>console.log(error)
      
    );
  }
  listarRequisitosOpcionales(){
    this.reqOption.getIdInscri(this.idUser,this.idConv).subscribe(
      data=>{
        console.log(data);
        this.idInscri = data[0].id;
        console.log("El id de inscri es " + this.idInscri );
        this.reqOption.listarDocumentosExGeneral(this.idInscri).subscribe(
          res=>{
            if( res == "Empty"){
              console.log(res);
            }else{
              this.listaDeReqOpcionales = res;
              console.log(res);
              
            }
          },error=>console.log(error)
          
          );
        }
    );
  }
  submit(idRend){
    this.notaRendimientoForm.controls['idRend'].setValue(idRend);
    console.log(this.notaRendimientoForm.value);
    return this.reqOption.modificarNotaRendimiento(this.notaRendimientoForm.value).subscribe(
      res=>{
        if(res=="Succes"){
          this.messgeAlert="Nota asignada correctamente"
          this.notaRendimientoForm.controls['nota'].reset();
          this.alert();
          this.doRefresh();
        }else{
          if(res=="Fail"){
            this.messgeAlert="La nota que quiere asignar es mayor a la Maxima Permitida"
            this.alert();
          }else{
            this.messgeAlert="El Postulante ya esta evaluado en este rendimiento"
            this.alert();
          }
        }  
      },error=> console.log(error)
    );
  }
  async alert() {
    const alert = await this.alertCtrl.create({
      message: this.messgeAlert,
      buttons: ['OK']
    });

    await alert.present();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      //event.target.complete();
    }, 2000);
  }
  refrescar(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.ngOnInit()
      event.target.complete();
    }, 2000);
  }
  async presentModal(requisito){
    this.puenteServicio.sendObjectSource(requisito);    
    const modal = await this.modalCtrl.create({
      component: ModalMeritosPage,
      cssClass: 'my-custom-class',
      componentProps: {}
    });
    return await modal.present();
  }

  asignarPuntos(idReq){
    this.puntosExpercienciaForm.controls['idReq'].setValue(idReq);
    console.log(this.puntosExpercienciaForm.value);
    return this.reqOption.modificarPuntoExperiencia(this.puntosExpercienciaForm.value).subscribe(
      res=>{
        switch(res){
          case "Succes": 
            this.messgeAlert="Puntos asignados correctamente"
            this.puntosExpercienciaForm.controls['punto'].reset();
            this.alert();
            this.doRefresh();
          break;
          case "Fail": 
            this.messgeAlert="Los puntos no coiciden con lo requerido por documento"
            this.alert();
          break;
          case "Max":
            this.messgeAlert="Los puntos superan el maximo permitido por requisito"
            this.alert();  
          break;
          case "1":
            this.messgeAlert="El postulante ya fue evaluado"
            this.alert();
          break;
        }
      },error=>console.log(error)
    );
  }
  goToPostulantes(){
    this.router.navigate(['/home/convocatorias/items-comision/usuarios']);
  }
}
