import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuariosPage } from './usuarios.page';

const routes: Routes = [
  {
    path: '',
    component: UsuariosPage
  },
  {
    path: 'meritos',
    loadChildren: () => import('./meritos/meritos.module').then( m => m.MeritosPageModule)
  },
  {
    path: 'conocimiento',
    loadChildren: () => import('./conocimiento/conocimiento.module').then( m => m.ConocimientoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsuariosPageRoutingModule {}
