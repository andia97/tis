import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {
  convocatorias: any
  constructor(
    private router: Router,
    public alertController: AlertController,
    public modalController: ModalController,
    private storageService: StorageService,
    private listarConvocatorias: ListConvocatorysService
  ) {
  }

  ngOnInit() {
    this.listConvocatorias()
  }
  listConvocatorias() {
    this.listarConvocatorias.getList().subscribe(
      res => {
        console.log(res);
        this.convocatorias = res
      }, error => console.log(error)
    );
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {}
    });
    return await modal.present();
  }

  ionViewWillEnter() {
    this.storageService.get(AuthConstants.AUTH).then(res => {
      // TODO es posible refactorizar
      if (res == null) {

      } else {
        this.router.navigate(['/home']);
      }
    });
  }

}
