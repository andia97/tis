import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import { MatTabsModule } from '@angular/material/tabs';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    NavBarComponent,
    
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    NavBarComponent,
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule,
    MatTabsModule,
    ReactiveFormsModule,
  ]
})
export class ComponentsModule { }