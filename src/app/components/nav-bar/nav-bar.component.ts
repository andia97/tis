import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { CodigoService } from 'src/app/services/codigo.service';
import { FormBuilder, Validators } from '@angular/forms';
import { CreateConvocatoryService } from 'src/app/services/create-convocatory.service';
import { AlertController } from '@ionic/angular';
import { ListConvocatorysService } from 'src/app/services/list-convocatorys.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent {

  codSecretaria: Boolean = false;
  codPostulante: Boolean = false;
  codComision: Boolean = false;
  codAdministrador: Boolean = false;
  constructor(
    private storageService: StorageService,
    private codigoService: CodigoService,
  ) {
    this.storageService.get(AuthConstants.TYPE_USER).then(res => {
      switch (res) {
        case 'Postulante': this.codPostulante = true; break;
        case 'Secretaria': this.codSecretaria = true; break;
        case 'Comision': this.codComision = true; break;
        case 'Administrador': this.codAdministrador = true; break;
        default: break;
      }
    });
  }

}
