import { Component, OnInit } from '@angular/core';
import {RedesSociales} from 'src/app/models/redes-sociales'
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {  
  redes:RedesSociales[] = [{
                            url:"https://www.facebook.com/labinfsis/",
                            logo:"assets/images/facebook.svg"
                            },
                            {
                              url:"https://www.instagram.com/isumss/?hl=es-la",
                              logo:"assets/images/instagram.png"
                            },
                            {
                              url:"http://www.umss.edu.bo/index.php/fac-ciencias-y-tecnologia/",
                              logo:"assets/images/fcyt.jpg"
                            },
                            {
                              url:"http://www.cs.umss.edu.bo/",
                              logo:"assets/images/logo.png"
                            }
                          ]
  constructor() { 
  }

  ngOnInit() {}

}
