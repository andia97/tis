import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AuthConstants } from 'src/app/config/auth-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() titulo: string;
  rol:String
  codAdministrador: Boolean = false;
  codPostulante: Boolean = false;
  codSecretaria:Boolean = false;
  codComision: Boolean = false;
  tipoDeUsuario:any
  constructor(private storageService:StorageService,
              private router:Router) { 
                this.storageService.get(AuthConstants.TYPE_USER).then(res => {
                  switch (res) {
                    case 'Postulante': this.codPostulante = true;this.rol="Postulante"; break;
                    case 'Secretaria': this.codSecretaria = true;this.rol="Secretaria"; break;
                    case 'Comision': this.codComision = true; this.rol="Comision";break;
                    case 'Administrador': this.codAdministrador = true; this.rol="Administrador";break;
                    default: break;
                  }
                });
              }

  ngOnInit() {}
    ionViewWillEnter() {
      this.storageService.get(AuthConstants.TYPE_USER).then(res => {
        this.tipoDeUsuario = res;
        console.log(this.tipoDeUsuario);
      });
    }

    async logout() {
      this.storageService.clear();
       await this.router.navigate(['/']).then(() => {
        window.location.reload();
      });
    }

}
