import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComisionService {

  constructor(private httpService:HttpService) { }

  listarRoles():Observable<any>{
    return this.httpService.get('listarRolComision')
  }
  registrarRolUsuario(rolForm:Object):Observable<any>{
    return this.httpService.post('registrarRolUsuario', rolForm)
  }
}
