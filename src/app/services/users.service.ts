import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private hhtpService: HttpService) { }

  getUserComision(){
    return this.hhtpService.get('usuariosComision');
  }
}
