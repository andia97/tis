import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TematicaService {

  constructor(private httpService:HttpService) { }

  añadirTematica(tematicaForm:object):Observable<any>{
    return this.httpService.post('crearTematica',tematicaForm);
  }
  listarTematicas(idItem):Observable<any>{
    return this.httpService.get('listarTematicas/'+ idItem);
  }
  eliminarTematica(idTematicaForm:any):Observable<any>{
    return this.httpService.post('eliminarTematica', idTematicaForm)
  }

  modificarNotaTematica(notaTematicaForm:any):Observable<any>{
    return this.httpService.post('aniadirNotaTematica', notaTematicaForm)
  }
  listarNotaTematica(idUser:any,idItem:any):Observable<any>{
    return this.httpService.get('listarNotaTematica/'+idUser+'/'+idItem)
  }
  sumarNotaUsuarioTematica(idUser:any,idItem:any):Observable<any>{
    return this.httpService.get('sumarNotasTematica/'+idUser+'/'+idItem)
  }
  sumaTotalTematica(idItem):Observable<any>{
    return this.httpService.get('sumaTematica/'+ idItem);
  }
}
