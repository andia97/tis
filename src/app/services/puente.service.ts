import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PuenteService {

  private objectSource = new BehaviorSubject<{}>({});
  private objectSource2 = new BehaviorSubject<{}>({});
  private listSource = new BehaviorSubject<any[]>([]);

  $getObjectSource = this.objectSource.asObservable();
  $getObjectSource2 = this.objectSource2.asObservable();
  $getListSource = this.listSource.asObservable();

  constructor() { }

  sendObjectSource(data: any) {
    this.objectSource.next(data);
  }
  sendObjectSource2(data: any) {
    this.objectSource2.next(data);
  }
  sendListSource(list: any[]) {
    this.listSource.next(list);
  }
}
