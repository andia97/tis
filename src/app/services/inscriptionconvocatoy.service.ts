import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class InscriptionconvocatoyService {

  constructor(private httpService:HttpService) { }
  inscription(inscriptionForm:any):Observable<any>{
    return this.httpService.post('inscriptionConvocatory',inscriptionForm)
  }
  inscribirPostulante(inscriptionForm:any):Observable<any>{
    return this.httpService.post('inscribir_postulante',inscriptionForm)
  }
}
