import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CodigoService {

  constructor(
    private httpService: HttpService
  ) { }

  getCodigo(tipoDeUsuario: string): Observable<any> {
    return this.httpService.get('codigo/' + tipoDeUsuario);
  }

  sendCodigo(postData: any): Observable<any> {
    return this.httpService.post('codigo', postData);
  }
}
