import { TestBed } from '@angular/core/testing';

import { TematicaService } from './tematica.service';

describe('TematicaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TematicaService = TestBed.get(TematicaService);
    expect(service).toBeTruthy();
  });
});
