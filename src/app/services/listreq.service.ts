import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListreqService {

  constructor(private httpService: HttpService) { }

  getList():Observable<any>{
    return this.httpService.get('listReqRequired')
  }
  addReq(reqForm:any):Observable<any>{
    return this.httpService.post('addReqRequired',reqForm)
  }
  getReqByConv(idConv:any):Observable<any>{
    return this.httpService.get('listAddRequired/'+idConv)
  }
  deleteReq(id:any):Observable<any>{
    return this.httpService.post('deleteReq',id)
  }
  addReqExtra(reqForm:any):Observable<any>{
    return this.httpService.post('addReqRequiredExtra',reqForm)
  }
  getReqByConvExtra(idConv:any):Observable<any>{
    return this.httpService.get('listAddRequiredExtra/'+idConv)
  }
  updateState(reqForm:any):Observable<any>{
    return this.httpService.post('updateState',reqForm)
  }
  habilitarPostulante(validarForm:any):Observable<any>{
    return this.httpService.post('habilitarPostulante',validarForm)
  }
  listarUsuariosPorItem(idConv:any,idItem:any):Observable<any>{
    return this.httpService.get('listarUsuariosItem/'+idItem+'/'+idConv);
  }
  inscripcionItem(incripcionItemForm:any):Observable<any>{
    return this.httpService.post('inscripcionItem',incripcionItemForm)
  }
  listarHabilitados(idConv:any):Observable<any>{
    return this.httpService.get('listaHabilitados/'+idConv);
  }
  listarInHabilitados(idConv:any):Observable<any>{
    return this.httpService.get('listaInhabilitados/'+idConv);
  }

  listarDocumentosPresentados(idInscripcion:any):Observable<any>{
    return this.httpService.get('listarRequisitosObligatoriosPresentados/'+idInscripcion);
  }
  validarDocumento(fileForm:any):Observable<any>{
    return this.httpService.post('actualizarEstadoDocumento',fileForm)
  }
}
