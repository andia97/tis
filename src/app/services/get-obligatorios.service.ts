import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetObligatoriosService {

  constructor(
    private httpService: HttpService
  ) { }

  getObligatorios(postData: any): Observable<any> {
    return this.httpService.post('getObligatorios', postData);
  }
}
