import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class CreateItemService {

  constructor(private httpService:HttpService) { }

  create(itemForm:Object):Observable<any>{
    return this.httpService.post('createItem',itemForm)
  }
}
