import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ListitemsService {

  constructor(private httpService:HttpService) { }
  getItems(idConv:any):Observable<any>{
    return this.httpService.get('listItemsConvocatory/'+idConv)
  }
  getItem(id:any):Observable<any>{
    return this.httpService.get('item/'+id)
  }

  listarItemsInscritos(idUser:any,idConv:any):Observable<any>{
    return this.httpService.get('listarItemesUsuario/'+idUser+'/'+idConv)
  }
}
