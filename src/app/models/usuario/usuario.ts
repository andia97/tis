export interface Usuario {
    id:number;
    userName:string;
    firstName:string;
    lastNameP:string;
    lastNameM:string;
    password:string;
    confirmPassword:string;
    email:string;
    phonen:number;
    dir:string;
    estado:string;
    tipo:string;
}
